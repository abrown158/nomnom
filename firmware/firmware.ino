#define PIN PD6
#define SWITCH PD3
#define STEP_PIN PD4
#define DIR_PIN PD5
#define LED LED_BUILTIN

#define DEFAULT_STEPS_PER_MM  6
#define DEFAULT_LENGTH_OF_CUT 50


void feed_and_chop  (void);
void motor_on       (void);
void motor_off      (void);
void feed_wire      (uint16_t steps);

void process_serial (void);
void process_serial_command (void);

static uint16_t num_of_lengths = 0;
static char cmd_buffer[32] ;
static uint8_t cmd_buffer_put_idx;

// settings that need to be eeprom based
static uint16_t steps_per_mm;
static uint16_t len_of_cut;

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(SWITCH, INPUT_PULLUP);
  
  
  num_of_lengths = 0;
  cmd_buffer_put_idx = 0;
  memset (cmd_buffer, 0, sizeof(cmd_buffer));

  steps_per_mm = DEFAULT_STEPS_PER_MM;
  len_of_cut = DEFAULT_LENGTH_OF_CUT;

  Serial.begin(9600);
  while (!Serial);// wait for serial port to connect. Needed for Native USB only

  Serial.println("nom nom is ready to munch");

}

void loop() {
  if (num_of_lengths > 0){
    feed_and_chop(); // this is blocking at the moment. need to change that
    --num_of_lengths;
  }

  process_serial();  
  
}

void feed_and_chop  (void){
     
  feed_wire( len_of_cut * steps_per_mm); 


  // assume motor is in home position. and move until swith
  // is depressed then keep moving until it is pressed again
  motor_on();
  
  while (!digitalRead(SWITCH)); //move motor until switch is released
  
  delay(100); // short delay before we starts sensing the switch has been pressed
              // this avoids reading the switch when it is bouncing
  
  while (digitalRead(SWITCH));  // move the motor until the switch is pressed
  
  motor_off();
  
  // wait for next go
  delay(500);   
}

void motor_on (void){
  digitalWrite(PIN, HIGH);
  digitalWrite(LED, HIGH);
}

void motor_off (void){
  digitalWrite(PIN, LOW);
  digitalWrite(LED, LOW);
  
}

void feed_wire (uint16_t steps){
  for (uint16_t i=0; i<steps; ++i){
    digitalWrite(STEP_PIN, HIGH); 
    delay(1);
    digitalWrite(STEP_PIN, LOW);
    delay(1);
  }
}

void process_serial (void){
    // service serial port
  if (Serial.available()){
    char c = Serial.read();
    cmd_buffer[cmd_buffer_put_idx] = c;
    cmd_buffer_put_idx++;

    Serial.write(c);
    
    if ( (c == '\r' || c == '\n') && cmd_buffer_put_idx > 1 )
    {
      cmd_buffer[cmd_buffer_put_idx-1]=0;
      cmd_buffer_put_idx = 0;
      process_serial_command();
    }
  }
}

void process_serial_command (void){
  uint8_t error = 1;
  char* tmp_ptr = cmd_buffer;
  uint16_t tmp_val = 0;

  tmp_ptr = cmd_buffer;

  if (strncmp(cmd_buffer, "S=", 2) == 0){
    Serial.write("\r\nFOUND S=\r\n");
    // find command and move past '='
    while (*tmp_ptr != '='){
      tmp_ptr++;
    }
    tmp_ptr++;

    if (sscanf(tmp_ptr, "%d", &tmp_val) == 1){
        error = 0;
        steps_per_mm = tmp_val;
    }
  }

  if (strncmp(cmd_buffer, "L=", 2) == 0){
    Serial.write("\r\nFOUND L=\r\n");
    while (*tmp_ptr != '='){
      tmp_ptr++;
    }
    tmp_ptr++;
    
    if (sscanf(tmp_ptr, "%d", &tmp_val) == 1){
        error = 0;
        len_of_cut = tmp_val;
    }
  }

  if (strncmp(cmd_buffer, "N=", 2) == 0){
    while (*tmp_ptr != '='){
      tmp_ptr++;
    }
    tmp_ptr++;
    
    if (sscanf(tmp_ptr, "%d", &tmp_val) == 1){
        error = 0;
        num_of_lengths = tmp_val;
    }
  }

  if (error == 1){
     Serial.write("\r\nERROR\r\n");
  }else{
     Serial.write("\r\nOK\r\n");
  }
}
