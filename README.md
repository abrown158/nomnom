

# NomNom

This is a repository for the automated cable measure and cut machine: NomNom

# Introduction & Why?
NomNom is an automated measure and cut machine for small cables. This project was created when I had a requirement to cut 1000's of cables to length for a home project.

With no access to a commercial machine I created this machine. It utilises readily available components, with some parts needing to be 3D printed.

I imagine that there are plenty of projects that have never got going due to some huge tedious task like this! I also think there are plenty of small companies out there who have found the cost of commercial equipment inaccessible who may be able to improve productivity and quality with a tool like this.

# Parts
At the moment there are three main parts to 3D print: 

 - The main mechanism that operates the snips
 - A riser for the feed stepper motor.
 - A cam that operates the snips

The other parts used are:

 - Donor wire snips
 - 3D printer extruder (with stepper motor)
 - Arduino
 - Stepper motor controller
 - Mosfet
 - Limit switch (stolen from an anetA8)
 - 12V motor with high torque worm gearbox
 - 12V power supply

I also used some MDF to mount everything on.

# Where are the assembly instructions?
These are coming. I am hoping pictures are enough for most. If the project gets enough interest then I will happily create some assembly instructions?

# What about .... ?
There are lots this project could do! There are also tons of improvements I would like to make. For now I have focused on getting something together that works and does the job it was intended for.

I would love to hear feature requests and ideas. Please check the active issues first, in case it has already been captured.

If you want to make your own then great! Go for it! Id love to hear about your experience and what problems you encountered.

Want to contribute? Perfect. Make contact via gitlab!

